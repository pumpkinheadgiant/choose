package main

import (
	"bufio"
	"log"
	"nijoh/lab/lab/path"
	"os"
	"strings"
)

func main() {
	output := bufio.NewWriter(os.Stdout)
	book := &path.Book{}
	book.LoadFromFile("dungeon_of_death.json")
	book.Render()

	input := bufio.NewReader(os.Stdin)

	inputValue, err := input.ReadString('\n')
	if err != nil {
		log.Print("big fat problem: " + err.Error())
	}
	inputValue = strings.Split(inputValue, "\n")[0]
	for inputValue != "quit" {
		book.HandleChoice(inputValue)
		book.Render()
		inputValue, err = input.ReadString('\n')
		inputValue = strings.Split(inputValue, "\n")[0]
	}
	if err != nil {
		log.Print("Game Over error: " + err.Error())
	} else {
		output.WriteString("Game over")
		output.Flush()
	}

}
