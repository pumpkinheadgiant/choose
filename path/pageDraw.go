package path

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"
)

var drawer PageDrawer

func init() {
	outFile := os.Stdout
	outWriter := bufio.NewWriter(outFile)

	drawer = &simplePageDrawer{
		output: outWriter,
	}
}

type simplePageDrawer struct {
	output *bufio.Writer
}

func (this *simplePageDrawer) DrawInventory(items []Thing, index int) {
	inventoryDescription := " nothing"

	if len(items) > 0 {
		descriptions := make([]string, 0)
		for _, thing := range items {
			description := fmt.Sprintf("%v (%c] Drop)", thing.currentState.Description, alphaMenu[index])

			descriptions = append(descriptions, description)
		}
		inventoryDescription = strings.Join(descriptions, ", ")
		inventoryDescription = inventoryDescription + "."
	}
	this.output.WriteString(fmt.Sprintf("You are carrying: %v%v", inventoryDescription, endParagraph))
	this.output.Flush()
}

func (this *simplePageDrawer) DrawGetItemPaths(items []Thing) {
	for index, item := range items {
		this.output.WriteString(fmt.Sprintf("%c] Pick up %v", alphaMenu[index], item.Name))
		this.output.WriteString(newLine)
	}
	if len(items) > 0 {
		this.output.WriteString(newLine)
		this.output.Flush()
	}
}

func (this *simplePageDrawer) DrawItems(things []Thing) int {

	for _, thingName := range things {
		this.output.WriteString(fmt.Sprintf("There is %v here. ", thingName.currentState.Description))
	}
	if len(things) > 0 {
		this.output.WriteString(endParagraph)
		this.output.Flush()

	}
	return len(things)
}

func (this *simplePageDrawer) DrawDunno(dunnoText string) {
	this.output.WriteString(fmt.Sprintf("%v'%v' is not a valid choice.%v", newLine, dunnoText, endParagraph))
}

func (this *simplePageDrawer) DrawFollow(follows []string) {
	this.output.WriteString(newLine)
	counter := 0

	for _, follow := range follows {
		counter++
		this.output.WriteString(follow)
		this.output.Flush()
		var delay = 600
		if counter%2 == 0 {
			delay = 400
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
	this.output.WriteString(endParagraph)
	this.output.Flush()
}

func (this *simplePageDrawer) DrawHeader(turnNumber int) {
	this.output.WriteString(newLine)
	this.output.WriteString(fmt.Sprintf("/--------------------%v", turnNumber))
	this.output.WriteString(newLine)
	this.output.Flush()
}

func (this *simplePageDrawer) DrawParagraphs(paragraphs []string) {
	this.output.WriteString(newLine)
	for _, paragraph := range paragraphs {
		this.output.WriteString(fmt.Sprintf("%v%v", paragraph, endParagraph))
	}
	this.output.Flush()
}

func (this *simplePageDrawer) DrawPagePaths(paths map[string]Path) {
	keysToSort := make([]string, 0)
	for pathKey, _ := range paths {
		keysToSort = append(keysToSort, pathKey)
	}
	sort.Strings(keysToSort)
	for _, key := range keysToSort {
		path := paths[key]
		this.output.WriteString(fmt.Sprintf("%v] %v%v", key, path.Text, newLine))
	}
	this.output.Flush()
}

func (this *simplePageDrawer) DrawPrompt() {
	this.output.WriteString("> ")
	this.output.Flush()
}
