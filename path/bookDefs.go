package path

type Book struct {
	Pages     []Page   `json:"pages"`
	Inventory []string `json:"inventory"`
	Things    []Thing  `json:"things"`

	currentPage Page
	currentTurn int

	pageByKeyMap   map[string]Page
	thingByNameMap map[string]Thing

	pathByInputKeyMap      map[string]Path
	thingPathByInputKeyMap map[string]ThingPath
}

type Thing struct {
	Name             string  `json:"name"`
	Carried          bool    `json:"carried"`
	InitialStateName string  `json:"initialStateName"`
	States           []State `json:"states"`
	currentState     State
	stateByNameMap   map[string]State
}

type State struct {
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Examine     string      `json:"examine"`
	ThingPaths  []ThingPath `json:"thingPaths"`
}

type ThingPath struct {
	Test   string `json:"test"`
	Text   string `json:"text"`
	Chosen Choice `json:"chosen"`
}

type Choice struct {
	NextState string   `json:"nextState"`
	Hide      []string `json:"hide"`
	Drop      []string `json:"drop"`
	Add       []string `json:"add"`
}

type Page struct {
	Key        string   `json:"key"`
	Paragraphs []string `json:"paragraphs"`
	Paths      []Path   `json:"paths"`
	IsTerminal bool     `json:"isTerminal"`
	Things     []string `json:"things"`
}

type Path struct {
	Key        string   `json:"key"`
	Text       string   `json:"text"`
	FollowText []string `json:"followText"`
}

type FollowText struct {
	PreDelay  int     `json:"preDelay"`
	Text      string  `json:"text"`
	PostDelay float32 `json:"postDelay`
}

type PageDrawer interface {
	DrawParagraphs(paragraphs []string)
	DrawItems(items []Thing) int
	DrawPagePaths(pathMap map[string]Path)
	DrawPrompt()
	DrawFollow(followText []string)
	DrawDunno(dunnoText string)
	DrawHeader(turnNumber int)
	DrawGetItemPaths(items []Thing)
	DrawInventory(items []Thing, thingIndex int)
}
