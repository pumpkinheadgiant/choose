package path

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

const newLine = "\n"
const endParagraph = newLine + newLine

func (this *Book) Render() {
	this.currentTurn++
	drawer.DrawHeader(this.currentTurn)
	drawer.DrawParagraphs(this.currentPage.Paragraphs)
	pageThings := this.getThingsList(pageThingSource)
	inventoryThings := this.getThingsList(inventoryThingSource)
	thingIndex := drawer.DrawItems(pageThings)
	drawer.DrawInventory(inventoryThings, thingIndex)
	drawer.DrawPagePaths(this.pathByInputKeyMap)
	drawer.DrawGetItemPaths(pageThings)
	drawer.DrawPrompt()
}

type thingSource string

var inventoryThingSource = thingSource("inventory")
var pageThingSource = thingSource("page")

func (this *Book) getThingsList(source thingSource) []Thing {
	thingNames := []string{}

	switch source {
	case pageThingSource:
		thingNames = this.currentPage.Things
	case inventoryThingSource:
		thingNames = this.Inventory
	default:
		log.Fatalf("Invalid thingsource: %v")
	}

	things := make([]Thing, 0)
	for _, thingName := range thingNames {
		thing, found := this.thingByNameMap[thingName]
		if !found {
			log.Fatalf("couldn't find thing with name '%v'", thingName)
		}
		things = append(things, thing)
	}
	return things
}

func (this *Book) HandleChoice(choice string) {

	handled := this.handlePageChoice(choice)
	if !handled {
		handled = this.handleItemChoice(choice)
		if !handled {
			drawer.DrawDunno(choice)
		}
	}

}
func (this *Book) handleItemChoice(choice string) bool {

	itemPath, found := this.thingPathByInputKeyMap[choice]
	if !found {
		return false
	}

	for _, itemToAdd := range itemPath.Chosen.Add {
		removedFromPage := false
		for _, pageThing := range this.currentPage.Things {
			if itemToAdd == pageThing {
				this.currentPage.Things = removeStringFromList(this.currentPage.Things, itemToAdd)
				this.Inventory = append(this.Inventory, itemToAdd)
				removedFromPage = true
				break
			}
		}
		if !removedFromPage {
			log.Fatalf(fmt.Sprintf("failed to remove '%v' from page", itemToAdd))
		}
		item, found := (this.thingByNameMap[itemToAdd])
		if !found {
			log.Fatalf("couldn't find item '%v'", itemToAdd)
		}
		item.Carried = true
		this.thingByNameMap[itemToAdd] = item
	}

	return true
}

func removeStringFromList(stringList []string, stringToDelete string) []string {
	filteredStrings := make([]string, 0)
	for _, stringInList := range stringList {
		if stringInList != stringToDelete {
			filteredStrings = append(filteredStrings, stringInList)
		}
	}
	return filteredStrings
}

func (this *Book) handlePageChoice(choice string) bool {
	path, found := this.pathByInputKeyMap[choice]
	if !found {
		return false
	}
	drawer.DrawFollow(path.FollowText)
	if path.Key != "" {
		this.currentPage = this.pageByKeyMap[path.Key]
		this.loadPage(this.currentPage)
		if this.currentPage.IsTerminal {
			log.Println("Hello Worms")
		}
	}
	return true
}

func (this *Book) LoadFromFile(name string) {
	bookFile := filepath.Join("resources", name)
	dungeonFile, err := os.Open(bookFile)
	if err != nil {
		log.Fatal("problem loading file named '%v': %v", name, err.Error())
	}
	defer dungeonFile.Close()

	bytes, err := ioutil.ReadAll(dungeonFile)
	json.Unmarshal(bytes, &this)

	this.pageByKeyMap = make(map[string]Page)
	for _, page := range this.Pages {
		this.pageByKeyMap[page.Key] = page
	}
	this.currentPage = this.pageByKeyMap["starting-page"]
	this.loadPage(this.currentPage)

	this.thingByNameMap = make(map[string]Thing)
	for _, thing := range this.Things {
		this.thingByNameMap[thing.Name] = thing
	}

	this.initializeThings()
}

func (this *Book) initializeThings() {
	for thingName, thing := range this.thingByNameMap {
		thing.stateByNameMap = make(map[string]State)
		for _, state := range thing.States {
			thing.stateByNameMap[state.Name] = state
		}
		state, found := thing.stateByNameMap[thing.InitialStateName]
		if !found {
			log.Fatal("Failed to initialize Thing '%v")
		}
		thing.currentState = state
		this.thingByNameMap[thingName] = thing
	}
}

const alphaMenu = `abcdefghijklmnopqrstuvwxyz`

func (this *Book) loadPage(page Page) {
	this.pathByInputKeyMap = make(map[string]Path)
	for i, path := range page.Paths {
		inputKey := fmt.Sprintf("%v", i+1)
		this.pathByInputKeyMap[inputKey] = path
	}
	this.thingPathByInputKeyMap = make(map[string]ThingPath)
	things := this.getThingsList(pageThingSource)
	for index, thing := range things {
		inputKey := fmt.Sprintf("%c", alphaMenu[index])
		thingPath := ThingPath{
			Text: fmt.Sprintf("%c] Pick up %v", inputKey, thing.Name),
			Chosen: Choice{
				NextState: "",
				Hide:      make([]string, 0),
				Drop:      make([]string, 0),
				Add:       []string{thing.Name},
			}}
		this.thingPathByInputKeyMap[inputKey] = thingPath
	}

}
